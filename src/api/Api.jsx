import axios from "axios";


export const api = axios.create({
    baseURL: "http://localhost:8080/api/v1",

    headers: {

        'Content-Type': ' application/json'
    },
})

api.interceptors.request.use((s) => {
    const token = localStorage.getItem("token")
    s.headers.Authorization = "Bearer " + token
    return s
})