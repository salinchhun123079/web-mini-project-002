import React from "react";
import firstpage from "../img/firstpage.jpg";
import { Link } from "react-router-dom";

const FirstPage = () => {
  return (
    <div>
      <div className="flex justify-between">
        <div className="text-center text-5xl flex mt-5 mx-20 ">
          <h1 className="">Task</h1>
          <h1 className="text-green-500 mx-2">Zone</h1>
        </div>
        <div className="m-4">
          <Link to={"/login"}>
            <button
              type="button"
              class="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 "
            >
              Login
            </button>
          </Link>
          <Link to={"/register"}>
            <button
              type="button"
              class="text-white bg-blue-700 hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 "
            >
              Register
            </button>
          </Link>

        </div>
      </div>

      <div class="g-6 flex h-full flex-wrap items-center justify-center lg:justify-between">
        <div class="shrink-1 mb-12 grow-0 basis-auto md:mb-0 md:w-9/12 md:shrink-0 lg:w-6/12 xl:w-6/12">
          <img src={firstpage} class="w-full" alt="Sample image" />
        </div>

        <div className="text-7xl ">
          <div className="flex flex-col text-end  mr-32">
            <div className="text-slate-500 hover:text-green-400">
              The fastest way to
            </div>
            <div className="text-green-500 hover:text-gray-600">
              complete you tasks web
            </div>
            <div className="text-slate-500 hover:text-red-500"> app</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FirstPage;
