import React from "react";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { signInUser, signUpUser } from "../redux/service/AuthService";
import { Formik, useFormik } from "formik";
import * as Yup from "yup";
import "flowbite";
import register from "../img/register.jpg";
import { Link, useNavigate } from "react-router-dom";

const Register = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch()
  const registerFun = (auth) => {
    dispatch(signUpUser(auth))
    navigate("/login");
  }

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string()
        .email("Invalid email address")
        .required("Please Input Email"),
      password: Yup.string()
        .required("Password is a required field")
        .min(6, "Password must be at least 8 characters"),
    }),
    onSubmit: (values) => {
      console.log(values)
      registerFun(values)
    },
  });

  return (
    <div>
      <div>
        <section className="h-screen">
          <div className="h-full">
            {/* <!-- Left column container with background--> */}
            <div className="g-6 flex h-full flex-wrap items-center justify-center lg:justify-between">
              <div className="shrink-1 mb-12 grow-0 basis-auto md:mb-0 md:w-9/12 md:shrink-0 lg:w-6/12 xl:w-6/12">
                <div className="text-4xl mt-20 ml-4">
                  <h1 className="">
                    Organize your life, one task at a time with Task
                  </h1>
                  <h1 className="text-green-400">Zone</h1>
                </div>

                <img src={register} className="w-full" alt="Sample image" />
              </div>

              {/* <!-- Right column container --> */}
              <div className="mb-12 md:mb-0 md:w-8/12 lg:w-5/12 xl:w-5/12 ">
                <form onSubmit={formik.handleSubmit}>
                  {/* <!--Sign in section--> */}
                  <div className="flex flex-row items-center justify-center lg:justify-start text-5xl my-20">
                    <h1>Sign up for Task </h1>
                    <h1 className="text-green-400 ml-4">ZONE</h1>
                    <br />
                    <br />
                  </div>

                  <div className="relative my-20" data-te-input-wrapper-init>
                    <input
                      className="peer block min-h-[auto] w-full rounded border-0 bg-transparent px-3 py-[0.32rem] leading-[2.15] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:text-neutral-200 dark:placeholder:text-neutral-200 [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0"
                      name="email"
                      type="email"
                      // id="email"
                      placeholder="Enter Email"
                      onChange={formik.handleChange}
                      value={formik.values.email}
                    />
                    {/* If validation is not passed show errors */}
                    {formik.errors.email &&
                      formik.touched.email &&
                      formik.errors.email}

                    <label

                      className="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[2.15] text-neutral-500 transition-all duration-200 ease-out peer-focus:-translate-y-[1.15rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[1.15rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-neutral-200"
                    >
                      Email address
                    </label>
                  </div>

                  {/* <!-- Password input --> */}
                  <div className="relative my-20" data-te-input-wrapper-init>
                    <input
                      className="peer block min-h-[auto] w-full rounded border-0 bg-transparent px-3 py-[0.32rem] leading-[2.15] outline-none transition-all duration-200 ease-linear focus:placeholder:opacity-100 data-[te-input-state-active]:placeholder:opacity-100 motion-reduce:transition-none dark:text-neutral-200 dark:placeholder:text-neutral-200 [&:not([data-te-input-placeholder-active])]:placeholder:opacity-0"
                      type="password"
                      name="password"
                      // id="password"
                      placeholder="Enter password"
                      onChange={formik.handleChange}
                      value={formik.values.password}
                    />
                    {/* If validation is not passed show errors */}

                    {formik.errors.password &&
                      formik.touched.password &&
                      formik.errors.password}

                    <label

                      className="pointer-events-none absolute left-3 top-0 mb-0 max-w-[90%] origin-[0_0] truncate pt-[0.37rem] leading-[2.15] text-neutral-500 transition-all duration-200 ease-out peer-focus:-translate-y-[1.15rem] peer-focus:scale-[0.8] peer-focus:text-primary peer-data-[te-input-state-active]:-translate-y-[1.15rem] peer-data-[te-input-state-active]:scale-[0.8] motion-reduce:transition-none dark:text-neutral-200 dark:peer-focus:text-neutral-200"
                    >
                      Password
                    </label>
                  </div>

                  {/* <!-- Login button --> */}
                  <div className="text-center lg:text-left my-20">
                      <button
                        type="submit"
                        className="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                      >
                        Register
                      </button>

                    {/* <!-- Register link --> */}
                    <p className="mb-0 mt-2 pt-1 text-sm font-semibold">
                      Already have an account?
                      <a
                        href="#!"
                        className="mx-2 text-danger transition duration-150 ease-in-out hover:text-danger-600 focus:text-danger-600 active:text-danger-700"
                      >
                        sign in
                      </a>
                    </p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </section>
      </div>
      {/* </Formik> */}
    </div>
  );
};

export default Register;
