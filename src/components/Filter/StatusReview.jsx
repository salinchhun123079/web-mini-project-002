import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { taskReview } from "../../redux/service/TaskService";

const StatusReview = () => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state.taskSlice.review);
  console.log(state);
  useEffect(() => {
    dispatch(taskReview());
  }, [dispatch]);

  return (
    <div>
      <div className="m-2 flex flex-wrap ">
        {state?.map((e, index) => (
          <div
            key={index}
            className={`${
              e.status === "Done"
                ? "bg-red-500"
                : e.status === "Review"
                ? " bg-green-500"
                : e.status ==="In_progress"
                ?"bg-purple-700"
                : "bg-gray-500"
            } card w-80 text-white rounded-lg m-1`}
          >
            <div className="flex justify-items-end  justify-end">
              <button
                type="button"
                className=" text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                data-modal-hide="defaultModal"
              >
                <svg
                  aria-hidden="true"
                  class="w-5 h-5"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    fill-rule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clip-rule="evenodd"
                  ></path>
                </svg>
                <span class="sr-only">Close modal</span>
              </button>
            </div>

            <div className="card-body ">
              <h2 className="justify-items-end card-title pl-5 text-3xl text-left uppercase ">
                {e.taskName}
              </h2>

              <p className="p-5 line-clamp-3">{e.description}</p>
              <br />
              <h2 className="card-title pl-5 text-2xl">{new Date(e.date).toLocaleDateString("en-CA", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                  })}</h2>
              <div className="flex card-actions justify-start items-center p-5 space-x-5">
                <button
                  type="button"
                 className="uppercase text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700"
                >
                  {e.status}
                </button>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default StatusReview;
