import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { fetchTodo } from '../redux/service/TaskService';
import { Link } from 'react-router-dom';
import 'flowbite'
import { Button, Modal } from 'flowbite-react';
// import { Button,Modal } from 'flowbite-react';
const Alltask = () => {
    const dispatch = useDispatch();

    const [isOpen, setIsOpen] = useState(false)
    const [updateOpen, setUpdateOpen] = useState(false)

    const openModal = () => {
        setIsOpen(true)
    }

    const closeModal = () => {
        setIsOpen(false)
    }


    const openUpdate = () => {
        setUpdateOpen(true)
    }

    const closeUpdate = () => {
        setUpdateOpen(false)
    }



    const state = useSelector((state) => state?.taskSlice.task);
    // console.log("store", state)
    useEffect(() => {
        dispatch(fetchTodo())
    }, [dispatch]);

    const getIdToUpdate = (e) => {
        console.log(e)
    }
    return (

        <div className="flex flex-wrap w-[1100px] h-auto justify-start gap-5 ">
            {state?.map((e, index) => (
                <div
                    key={index} onClick={() => getIdToUpdate(e.taskId)}
                    className={`${e?.status === "Done"
                            ? "bg-[#87ab69]"
                            : e?.status === "Review"
                                ? " bg-[#fddf8e]"
                                : e?.status === "In Progress"
                                    ? "bg-[#006a89]"
                                    : "bg-[#c8c8c8]"
                        } card w-80 rounded-3xl m-1`}
                >
                    <div>


                        <div onClick={openUpdate} className="mt-[20px] cursor-pointer">
                            {/* date */}
                            <h2 className="card-title pl-5 text-2xl mb-[15px] font-bold">{new Date(e?.date).toLocaleDateString("en-CA", {
                                weekday: "long",
                                month: "long",
                                day: "numeric",
                            })}</h2>
                            <h2 className="justify-items-end font-bold card-title pl-5 text-2xl text-left">
                                {e?.taskName}
                            </h2>

                            <p className="p-5 line-clamp-3 text-grey-100gkit  text-[20px]">{e?.description}</p>
                            <br />
                        </div>


                        <Button id='btDel' style={{
                            backgroundColor: 'none',
                            width: '30px',
                            height: '30px',
                            text: 'black'
                        }} className=' rounded-full bg-transparent hover:bg-transparent border-transparent absolute mt-[-170px] ml-[260px]' onClick={openModal}>
                            <svg xmlns="http://www.w3.org/2000/svg" class="stroke-current flex-shrink-0 h-7 w-7" fill="none" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z" /></svg>

                        </Button>
                        {/* button delete task  */}

                        {/* button status */}
                        <div className="flex card-actions justify-start items-center p-5 space-x-3 rounded-full" aria-hidden="true">
                            <button data-modal-target="false" data-modal-toggle="false"

                                className={` uppercase h-10 w-32  bg-white font-bold rounded-2xl text-sm px-3 text-center mr-2 mb-2`}
                            >
                                {e?.status}
                            </button>
                        </div>


                    </div>

                </div>
            ))
            }


            {/* popup modal */}
            <React.Fragment>
                <Modal
                    show={isOpen}
                    size="md"
                    popup={true}
                    onClose={closeModal}
                >
                    <Modal.Header />
                    <Modal.Body>
                        <div className="text-center">
                            {/* <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-gray-400 dark:text-gray-200" /> */}
                            <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                                Are you sure you want to delete this product?
                            </h3>
                            <div className="flex justify-center gap-4">
                                <Button
                                    color="failure"
                                    onClick={closeModal}
                                >
                                    Yes, I'm sure
                                </Button>
                                <Button
                                    color="gray"
                                    onClick={closeModal}
                                >
                                    No, cancel
                                </Button>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
            {/* update modal */}

            <React.Fragment>
                <Modal
                    show={updateOpen}
                    size="md"
                    popup={true}
                    onClose={closeUpdate}
                >

                    <Modal.Header>
                        <div className='flex'>
                            <h1 className='ml-[12px] text-[20px] font-bold'>Task Name</h1>
                            <h1 className='ml-[200px] text-[20px] font-bold text-red-700'>Date</h1>
                        </div>

                    </Modal.Header>
                    <Modal.Body>
                        <div className="space-y-6 rounded-3xl">
                            <div className='flex'>
                                <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                                    Category :
                                </p>
                                <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                                    TaskName
                                </p>
                                <h1 className='text-[#54ddd4] font-bold ml-[150px]'>
                                    Review
                                </h1>
                            </div>
                            <hr className='h-10px]' />
                            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
                                With less than a month to go before the European Union enacts new consumer privacy laws
                            </p>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div className='flex ml-[190px]'>
                            <Link to={"/home/updateTask"}>
                                <Button onClick={closeUpdate} className='bg-[#54ddd4] hover:bg-[#2cbeb5] mr-3'>
                                    UPDATE
                                </Button>
                            </Link>
                            <Button
                                onClick={closeUpdate}
                                color="gray"
                            >
                                CLOSE
                            </Button>
                        </div>

                    </Modal.Footer>
                </Modal>
            </React.Fragment>

        </div >




    )
}

export default Alltask