import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { categoryAll, deleteCategory } from "../../redux/service/CategoryService";
import AddCategory from "./AddCategory";
import UpdateCategory from "./UpdateCategory";

const CategoryGetAll = () => {
  const [show, setShow] = useState(false);
  const [showUp, setShowUp] = useState(false);
  const [idUpdate, setIdUpdate] = useState();

  const dispatch = useDispatch();

  const state = useSelector((state) => state.categorySlice.category);


  useEffect(() => {
    dispatch(categoryAll());
  }, [dispatch]);

  const getId = (id) => {
    setIdUpdate(id);
  };

  const deleteCategoryHandler = (id) => {
    console.log(id);
    dispatch(deleteCategory(id));

    // setShowUp(false);
  };


  return (
    <div>
      <div className="m-5 flex flex-wrap ">
        <div class="w-full max-w-sm bg-white border-l-4 border-[#4ecdc4] rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 m-5">
          <div class="flex flex-col items-center">
            <div className="card-body ">
              <h2 className="justify-items-end card-title text-left text-3xl font-bold ">
                Category
              </h2>
            </div>
            {/* button add category */}
            <button
              class="inline-block text-gray-100 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-gray-200 dark:focus:ring-gray-700 rounded-lg text-sm p-1.5"
              type="button"
              onClick={() => setShow(!show)}
            >
              <svg
                className="w-16 h-16 text-white-500 bg-[#4ecdc4] rounded-full"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
                aria-hidden="true"
              >
                <path
                  clip-rule="evenodd"
                  fill-rule="evenodd"
                  d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z"
                ></path>
              </svg>
            </button>
          </div>
          {show ? <AddCategory show={show} setShow={setShow} /> : null}
        </div>

        {/* Loop card category */}
        {state?.map((e, index) => (
          <div
            key={index}
            class="w-full max-w-sm bg-white border border-l-4 border-[#4ecdc4] rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 m-5"
          >

            <div class="flex flex-col">
              <div className="card-body ">
                <div className="flex justify-end">
                  <div className="dropdown dropdown-bottom">
                    <label tabIndex={0}>
                      <svg
                        class="w-6 h-6"
                        aria-hidden="true"
                        fill="currentColor"
                        viewBox="0 0 20 20"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path d="M6 10a2 2 0 11-4 0 2 2 0 014 0zM12 10a2 2 0 11-4 0 2 2 0 014 0zM16 12a2 2 0 100-4 2 2 0 000 4z"></path>
                      </svg>
                    </label>
                    <ul
                    onClick={() => getId(e.categoryId)}
                      tabIndex={0}
                      className="dropdown-content menu p-2 shadow bg-base-100 rounded-box w-52"
                    >
                      <li >
                        <a
                          href="#"
                          class="block px-4 py-2 text-xl text-gray-700 hover:bg-[#4ecdc4] dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
                          onClick={() => setShowUp(!showUp)}
                        >
                          Edit
                        </a>
                      </li>

                      <li
                      >
                        <a
                          href="#"
                          class="block px-4 py-2 text-xl text-red-600 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white"
                          onClick={()=>deleteCategoryHandler(e.categoryId)}
                       >
                          Delete
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <h1 className=" card-title pl-2 flex justify-start text-3xl text-left uppercase ">
                    {e.categoryName}
                  </h1>
                <h2 className="card-title pl-5 text-2xl">
                
                  {new Date(e.date).toLocaleDateString("en-CA", {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric",
                  })}
                </h2>
              </div>
            </div>
          </div>
        ))}
      </div>
      {showUp ? (
        <UpdateCategory
          showUp={showUp}
          setShowUp={setShowUp}
          idUpdate={idUpdate}
        />
      ) : null}
    </div>
  );
};

export default CategoryGetAll;
