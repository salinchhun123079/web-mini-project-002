import axios from 'axios'
import { Link } from 'react-router-dom'
import React, { Children, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { addNewTask } from '../redux/service/TaskService';
import { useNavigate } from "react-router-dom";

function Task() {

    //declare variable to add new task
    const [date, setDate] = useState("");
    const [taskName, setTaskName] = useState("");
    const [description, setDescription] = useState("");
    const [status, setStatus] = useState("");
    const [categoryId, setCategoryId] = useState(3);
    const dispatch = useDispatch();

    
    const navigate = useNavigate();

    const addTaskHandler = (e) => {
        e.preventDefault();
        
        // change format date
        const dateConvert = new Date(date)
        let newDate = dateConvert.toISOString();
        const body = {
            newDate, taskName, status, description, categoryId
        }
        dispatch(addNewTask(body))
        navigate("/home/board");
    }

    useEffect(() => {
        dispatch(addNewTask())
    }, [dispatch]);

    

    const[formData, setFormData] = useState({
        dates: "",
        categorys: "",
        statuss: "",
        taskNames: "",
        descriptions: "",
    });

    const [data, setData]=useState([]);
    
    const { dates, categorys,statuss,taskNames,descriptions} = formData;
    const handleChange = (e)=>{
        setFormData9({...formData, [e.target.name]:e.target.value})
    }

    const handleDelete = (index) =>{
        const newData = data.filter((item, i) => i !== index)
        setData(newData)
    }

    const handleUpdate = (updateId) =>{
        axios.get(`http://localhost:8080/api/v1/addNewTask/${updateId}`)
        .then(res => {
            setFormData(res.data)
        })
        .catch(err => console.log(err))

    }

    return (
        <div className='ml-[150px] mt-[80px]'>
            <div>
                <div>
                    <h1 className='text-[50px] mb-[30px] font-bold'>Add New Task</h1>
                    <form>
                        <div class="mb-[-100px] flex justify-start">
                            <div className='mb-3 mr-10'>
                                <label for="date" class="block mb-2 text-[20px] font-bold text-gray-900 dark:text-white ">Date</label>
                                <input value={date} type="datetime-local" class="h-[45px] shadow-md bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-[280px] p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light" placeholder="name@flowbite.com" required
                                    onChange={(e) => setDate(e.target.value)}
                                />

                            </div>
                            <div className='mb-3 mr-10'>
                                <label for="category" class="block mb-2 text-[20px] font-bold text-gray-900 dark:text-white">Category</label>
                                <select onChange={(e) => setCategoryId(e.target.value)} id="small" class="shadow-md block w-[280px] h-[45px] p-2 mb-6 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected disabled>Choose Category</option>
                                    <option value=""></option>
                                    <option value=""></option>
                                    <option value=""></option>
                                </select>
                            </div>
                            <div className='mb-2 mr-10'>
                                <label for="status" class="block mb-2 text-[20px]  text-gray-900 dark:text-white font-bold">Status</label>
                                <select onChange={(e) => setStatus(e.target.value)} class="shadow-md block w-[280px] h-[45px]  p-2 mb-6 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                                    <option selected disabled>Choose Status</option>
                                    <option value="Done">Done</option>
                                    <option value="Review">Review</option>
                                    <option value="Progress">Progress</option>
                                    <option value="Not-yet">Not yet</option>
                                </select>
                            </div>
                            {/* <div className=''>
                                <svg class="w-60 h-60 text-gray-400 dark:text-gray-500 group-hover:text-blue-600 dark:group-hover:text-blue-500" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z"></path><path fill-rule="evenodd" d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z" clip-rule="evenodd"></path></svg>
                            </div> */}
                            <img src="Images/tasks.png" alt="" className='w-[200px] h-[200px]' />
                        </div>
                        <div class="-mt-10">
                            <label for="Title" class="block  mb-2 text-[20px] text-gray-900 dark:text-white font-bold">Title</label>
                            <input onChange={(e) => setTaskName(e.target.value)} type="text" class="w-[920px] h-[45px] shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 dark:shadow-sm-light" />
                        </div>
                        <div class="mt-10">
                            <label for="message" class="block mb-2 text-[20px] font-bold text-gray-900 dark:text-white">Description</label>
                            <textarea onChange={(e) => setDescription(e.target.value)} id="message" rows="4" class="h-[200px] w-[1250px] block p-2.5 text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Leave a comment..."></textarea>
                        </div>
                        <div className='mt-6'>
                            <button type="button" class="text-gray-900 mt-3 bg-white border border-gray-300 focus:outline-none mr-[30px] hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-8 py-4 mb-2 dark:bg-gray-800 dark:text-white dark:border-gray-600 dark:hover:bg-gray-700 dark:hover:border-gray-600 dark:focus:ring-gray-700">Cancel</button>
                            
                                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-8 py-4 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                    onClick={addTaskHandler}
                                >Create</button>  
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Task