
import LeftSide from './LeftSide'
import RightSide from './Content'
import Content from './Content'
import { Outlet } from 'react-router-dom'


function Index() {
  return (
    <div class="grid grid-cols-5">
      <div>
        <LeftSide/> 
      </div>
      <div className='col-span-4'>
        <Outlet/>
      </div>
      
    </div>
  )
}

export default Index