import React from 'react'
import { Link } from 'react-router-dom'
import done from '../../public/Images/done.jpg'
function LeftSide() {

    return (
        <div className='bg-[#eafcfa] h-full'>
            <div className='ml-[35px] pt-[50px]'>
                <div className='flex items-center '>
                    <div class="relative inline-flex items-center justify-center w-[50px] h-[50px] overflow-hidden bg-[#4ecdc4] rounded-full dark:bg-gray-600">
                        <span class="font-medium text-gray-600 dark:text-gray-300 text-white text-3xl">Z</span>
                    </div>
                    <h1 className='ml-[25px] text-3xl font-bold'>Workspace</h1>
                </div>
                <div>

                </div>
                <div className='mt-[25px] ml-[60px] h-[500px]'>

                    {/* link to board */}
                    <Link to={"/home/board"}>
                        <button className='mb-[35px] flex text-2xl items-center font-bold'>
                            <img className='h-[30px] w-[30px]' src={done} />
                            <h1 className='ml-[25px]'>Boards</h1>
                        </button>
                    </Link>
                    <Link to={"/home/getAllCategory"}>
                        <button className='mb-[10px] flex text-2xl items-center font-bold'>
                            <img className='h-[30px] w-[30px]' src="Images/menu.jpg" />
                            <h1 className='ml-[25px]'>Category</h1>
                        </button>
                    </Link>
                    {/* dropdown */}
                    <div class="h-full px-1 py-4 overflow-y-auto ml-[7px]">
                        <ul class="font-medium">
                            <li>
                                <button type="button" class="flex items-center w-full mb-[20px] text-gray-900 transition duration-75 rounded-lg group dark:text-white" aria-controls="dropdown-example" data-collapse-toggle="dropdown-example">
                                    <span class="flex-1 ml-3 text-left whitespace-nowrap text-2xl font-bold" sidebar-toggle-item>Status</span>
                                    <svg sidebar-toggle-item class="w-8 h-8 mr-[110px]" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                                </button>
                                <div className='ml-[30px]'>
                                    <ul id="dropdown-example" class="hidden py-2 space-y-2">
                                        <div>
                                            <Link to={"/home/statusDone"}>
                                                <button
                                                    className='mb-[35px] flex text-2xl items-center font-bold'>
                                                    <img className='h-[30px] w-[30px]' src="Images/done.jpg" />
                                                    <h1 className='ml-[25px]'>Done</h1>
                                                </button>
                                            </Link>

                                        </div>


                                        <li>
                                            <div>
                                                <Link to={"/home/statusProgress"}>
                                                    <button className='mb-[35px] flex text-2xl items-center font-bold'>
                                                        <img className='h-[30px] w-[30px]' src="Images/progress.png" />
                                                        <h1 className='ml-[25px]'>Progress</h1>
                                                    </button>
                                                </Link>
                                            </div>


                                        </li>
                                        <li>
                                            <div>
                                                <Link to={"/home/statusReview"}>
                                                    <button className='mb-[35px] ml-[4px] flex text-2xl items-center font-bold'>
                                                        <img className='h-[30px] w-[30px]' src="Images/review.png" />
                                                        <h1 className='ml-[20px]'>Reviews</h1>
                                                    </button>
                                                </Link>
                                            </div>

                                        </li>
                                        <li>
                                            <Link to={"/home/statusNotYet"}>
                                                <button className='mb-[20px] ml-[5px] flex text-2xl items-center font-bold'>
                                                    <img className='h-[30px] w-[25px]' src="Images/not yet.jpg" />
                                                    <h1 className='ml-[26px]'>Not Yet</h1>
                                                </button>
                                            </Link>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <button className='flex text-2xl items-center font-bold'>
                                    <img className='h-[30px] w-[30px]' src="Images/logout.png" />
                                    <h1 className='ml-[25px] text-[#e00901]'>Logout</h1>
                                </button>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>
    )
}

export default LeftSide