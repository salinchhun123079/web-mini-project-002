import React from 'react'
import Task from './Task'
import { Link } from 'react-router-dom'
import { useEffect } from 'react'
import { fetchTodo } from '../redux/service/TaskService'
import { useDispatch, useSelector } from 'react-redux'
import Alltask from './Alltask'
import Login from '../login/Login'
import Register from '../login/Register'

import CategoryGetAll from './category/CategoryGetAll'
function Content() {
   

    return (
        <div className='ml-[140px] mt-[60px]'>
           
            <div className='text-3xl font-bold flex mb-[15px]'>
                <h1 className='text-[60px]'>All your boards</h1>
                <Link to={"/home/addNewTask"}>
                    <button type="button" class="ml-[550px] text-white bg-[#4ecdc4] font-medium rounded-2xl text-[30px] px-7 py-4 text-center mr-2 mb-2">Add new task</button>
                </Link>
            </div>
       
            {/* {state?.payload?.map((e, index) =>(
                <div key={index}>
                    <h1>{e.categoryName}</h1>
                    <h1>{e.date}</h1>
                </div>
            ))} */}
            <Alltask/>
            <div>
                {/* <Login/>
                <Register/> */}
            </div>
        </div>
    )
}

export default Content