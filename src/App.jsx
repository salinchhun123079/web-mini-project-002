import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import Index from './components/Index'
import Content from './components/Content'
import LeftSide from './components/LeftSide'
import Task from './components/Task'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import TaskUpdate from './components/TaskUpdate'
import CategoryGetAll from './components/category/CategoryGetAll'
import StatusDone from './components/Filter/StatusDone'
import StatusReview from './components/Filter/StatusReview'
import StatusNotYet from './components/Filter/StatusNotYet'
import StatusProgress from './components/Filter/StatusProgress'
import Register from './login/Register'
import Login from './login/Login'
import FirstPage from './login/FirstPage'
// import 'rsuite/dist/rsuite.mtin.css';

function App() {

  const [count, setCount] = useState(0)

  return (
    <BrowserRouter>


      <Routes>
        <Route path='/' element={<FirstPage />}></Route>
        <Route path='/login' element={<Login />}></Route>
        <Route path='/register' element={<Register />}></Route>

        <Route path='/home/' element={<Index />}>
          <Route path='board' element={<Content />}></Route>
          <Route path='addNewTask' element={<Task />}></Route>
          <Route path='updateTask' element={<TaskUpdate />}></Route>
          <Route path='getAllCategory' element={<CategoryGetAll />}></Route>
          <Route path='statusDone' element={<StatusDone />}></Route>
          <Route path='statusReview' element={<StatusReview />}></Route>
          <Route path='statusNotYet' element={<StatusNotYet />}></Route>
          <Route path='statusProgress' element={<StatusProgress />}></Route>
        </Route>
      </Routes>
    </BrowserRouter>


  )
}

export default App
