import { configureStore } from "@reduxjs/toolkit";
import CategorySlide from "./slice/CategorySlide";
import taskSlide from "./slice/TaskSlide";
import authSlice from "./slice/AuthSlide";

export const store = configureStore({
        reducer:{
            taskSlice: taskSlide,
            categorySlice:CategorySlide,            
            user: authSlice,
        }
    }
)

export default store