import { createSlice } from "@reduxjs/toolkit";
import { addCategory, categoryAll, deleteCategory, updateCategory } from "../service/CategoryService";

const initialState={
    loading:false,
    category:[],
    error:""
    };

    const categorySlice= createSlice({
        name:"task",
        initialState,
        reducers : {},
        extraReducers: (builder) => {
          // category
          builder.addCase(categoryAll.pending, (state) => {
            state.loading = true;
          });
          builder.addCase(categoryAll.fulfilled, (state, action) => {
            state.loading = false;
            state.category= action.payload;
          });
          builder.addCase(categoryAll.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message;
          });

          // AddCategory
          builder.addCase(addCategory.pending, (state) => {
            state.loading = true;
          });
          builder.addCase(addCategory.fulfilled, (state, action) => {
            state.loading = false;
            state.category.push(action.payload)
          });
          builder.addCase(addCategory.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message;
          });

          // Update Category
          builder.addCase(updateCategory.pending, (state) => {
            state.loading = true;
          });
          builder.addCase(updateCategory.fulfilled, (state, action) => {
            state.loading = false;
            const indexUpdate = action.payload
            console.log(indexUpdate);
            const index = state.category.findIndex((data) => data.categoryId == indexUpdate.categoryId)
            console.log("index",index);
            if(index !== -1){
              state.category[index] = indexUpdate
            }
          });
          builder.addCase(updateCategory.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message;
          });

          // Delete Category
          builder.addCase(deleteCategory.pending, (state) => {
            state.loading = true;
          });
          builder.addCase(deleteCategory.fulfilled, (state, action) => {
            state.loading = false;
            const indexDelete = action.payload
            console.log("delete",indexDelete);
            state.category = state.category.filter(data => data.categoryId !== indexDelete)
            console.log(state.category);
           
          });
          builder.addCase(deleteCategory.rejected, (state, action) => {
            state.loading = false;
            state.error = action.error.message;
          });
          
        }
    });

    export default categorySlice.reducer