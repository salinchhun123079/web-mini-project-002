import { createSlice } from "@reduxjs/toolkit";
import { addNewTask, fetchTodo } from "../service/TaskService";
import { taskDone, taskIn_progress, taskNot_Yet, taskReview } from "../service/TaskService";

const initialState = {
  loading: false,
  task: [],
  addTask: [],
  done:[],
  review: [],
  progress: [],
  not_yet: [],
  error: "",
};

const taskSlice = createSlice({
  name: "task",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchTodo.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchTodo.fulfilled, (state, action) => {
      console.log("action", action.payload);
      state.loading = false;
      state.task = action.payload;
    })
    builder.addCase(fetchTodo.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });
    // add task
    builder.addCase(addNewTask.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(addNewTask.fulfilled, (state, action) => {
      state.loading = false;
      state.task.push(action.payload);
    })
    builder.addCase(addNewTask.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

    // Done
    builder.addCase(taskDone.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(taskDone.fulfilled, (state, action) => {
      state.loading = false;
      state.done = action.payload;
    });
    builder.addCase(taskDone.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

    // TaskReview

    builder.addCase(taskReview.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(taskReview.fulfilled, (state, action) => {
      state.loading = false;
      state.review = action.payload;
    });
    builder.addCase(taskReview.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

    // progress
    builder.addCase(taskIn_progress.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(taskIn_progress.fulfilled, (state, action) => {
      state.loading = false;
      state.progress = action.payload;
    });
    builder.addCase(taskIn_progress.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

    // Not_yet
    builder.addCase(taskNot_Yet.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(taskNot_Yet.fulfilled, (state, action) => {
      state.loading = false;
      state.not_yet = action.payload;
    });
    builder.addCase(taskNot_Yet.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });
  },


});

export default taskSlice.reducer;