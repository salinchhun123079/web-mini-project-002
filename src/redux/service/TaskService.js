import { createAsyncThunk } from "@reduxjs/toolkit";
import { api } from "../../api/Api";


export const fetchTodo = createAsyncThunk("task/fetchTodo", async () => {
    try {
        const response = await api.get("/getAllTaskCurrentUser?page=1&size=12&asc=false&desc=false",
        )
        console.log("res", response.data);
        return response.data.payload;
    } catch (err) {
        console.log("err", err);
    }
});

export const addNewTask = createAsyncThunk("task/addNewTask", async (body) => {
    try {
        const newObject = {
            taskName: body.taskName,
            description: body.description,
            date: body.date,
            status: body.status,
            categoryId: body.categoryId
        }
        const response = await api.post("/addNewTask", newObject)
        console.log("res", response.data);
        return response.data.payload;
    } catch (err) {
        console.log("err", err);
    }
});


export const taskDone = createAsyncThunk('task/taskDone', async()=>{
    try{
        const response = await api.get("/status/users?status=Done",

        );
        return response.data.payload;
    }catch(err){
        console.log(err);
    }
});

// Task Review
export const taskReview = createAsyncThunk('task/taskReview', async()=>{
    try{
        const response = await api.get("/status/users?status=Review",
        
        );
        return response.data.payload;
    }catch(err){
        console.log(err);
    }
});
// Progress
export const taskIn_progress = createAsyncThunk('task/taskIn_progress', async()=>{
    try{
        const response = await api.get("/status/users?status=In Progress",
      
        );
        return response.data.payload;
    }catch(err){
        console.log(err);
    }
});
// Not yet
export const taskNot_Yet = createAsyncThunk('task/taskNot_Yet', async()=>{
    try{
        const response = await api.get("/status/users?status=Not yet",
       
        );
        return response.data.payload;
    }catch(err){
        console.log(err);
    }
});


const taskService={
taskDone,
taskIn_progress,
taskReview,
taskNot_Yet,
fetchTodo,
addNewTask
}

export default taskService