import { createAsyncThunk } from "@reduxjs/toolkit";
import { api } from "../../api/Api";

export const categoryAll = createAsyncThunk('category/categoryAll', async()=>{
    try{
        const response = await api.get("/allCategoryCurrentUser?page=1&size=10&asc=false&desc=false",

        );
        return response.data.payload;
    }catch(err){
        console.log(err);
    }
});

export const addCategory = createAsyncThunk('category/AddNewCategory', async(body)=>{
    try{
        const newObject = {
            categoryName: body.categoryName,
        }
        const response = await api.post("/addNewCategory",
        newObject
        );

        return response.data.payload;
    }catch(err){
        console.log(err);
    }
});

export const updateCategory = createAsyncThunk('category/updateCategory', async(body)=>{
    try{
        const newObject = {
            categoryName: body.categoryName,
        }
        const response = await api.put(`/updateCategoryById${body.idUpdate}`,
        newObject
        );
        // console.log("y",response);
        return response.data.payload;
    }catch(err){
        console.log(err);
    }
});
export const deleteCategory = createAsyncThunk('category/deleteCategory', async(body)=>{
    console.log("body:",body);
    try{
        const response = await api.delete(`/deleteCategoryCurrentUser${body}`
        
        );
        console.log("res delete:",response);
        return body;
    }catch(err){
        console.log(err);
    }
});




const categoryService={
    categoryAll,
    addCategory,
    updateCategory,
    deleteCategory
}

export default categoryService
