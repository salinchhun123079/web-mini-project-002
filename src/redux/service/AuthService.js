import {  createAsyncThunk} from "@reduxjs/toolkit";
import { api } from "../../api/Api";


export const signUpUser = createAsyncThunk("signupuser", async (body) => {
  console.log("body", body);
  try {
    const response = await api.post(
      "/auth/addNewUser", body,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    console.log(response);
  } catch (err) {
    console.log(err);
  }
});

export const signInUser = createAsyncThunk("signinuser", async (body) => {
  console.log("body", body);
  try {
    const response = await api.post(
      "/auth/authenticate",  body,
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    localStorage.setItem("token",response.data.token)
    console.log(response);
  } catch (err) {
    console.log(err);
  }
});

const AuthService={
    signInUser,
    signUpUser
}

export default AuthService
